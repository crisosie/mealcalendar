# -*- coding: utf-8 -*-
"""
Created on Fri Sep 17 22:31:55 2021

@author: Cristian
"""
import json
import yaml
from yaml.loader import SafeLoader

def readJSON(path):
    """Reads JSON file

    Args:
        path (str): path to be parsed

    Returns:
        (dict): json as dict
    """
    parsed_dict = dict()        
    with open(path, "r") as json_file:
        parsed_dict = json.load(json_file)
    return parsed_dict

def writeJSON(path, data):
    with open(path, "w") as json_file:
        json.dump(data, json_file)

def readYAML(path):
    """Reads YAML file

    Args:
        path (str): path to be parsed

    Returns:
        (dict): YAML as dict
    """
    parsed_dict = dict()        
    with open(path, "r") as yaml_file:
        parsed_dict = yaml.load(yaml_file, Loader=SafeLoader)
    return parsed_dict

def writeYAML(path, data):
    with open(path, "w") as yaml_file:
        yaml.safe_dump(data, yaml_file, sort_keys=False, encoding='utf-8', allow_unicode=True)

if __name__ == '__main__':
   test_dict = readJSON(r"D:\git\projects\meal_calendar\json\meals.json")