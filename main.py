# -*- coding: utf-8 -*-
"""
Created on Fri Sep 17 22:31:55 2021

@author: Cristian
"""
from datetime import date, timedelta
import json
import random
import utils
import os

dirname = os.path.dirname(__file__)

class RandomMeal():
    """Generates random meal.
    """    
    def __init__(self, meals_path):
        """Initializes random meal class.

        Args:
            meals_path (str): path to the file that holds the meals
        """        
        self.meals_dict = dict()
        self.meal_list = list()
        self.meals_path = meals_path
        self.meals_dict = utils.readYAML(meals_path)
        self.initMealLists()
        return
    
    def initMealLists(self):
        """Initializes meals lists.
        """        
        self.breakfast_list = self.setMealList("desayuno")
        self.dinner_list = self.setMealList("comida")
        self.supper_list = self.setMealList("cena")
    
    def getMeal(self, meal_type):
        """Gets random meal for a specific meal type

        Args:
            meal_type (str): meal type/meal time

        Returns:
            str: meal name
        """        
        if not self.meals_dict:
            return ""
        
        meal_list = self.getMealList(meal_type)
        if not meal_list:
            return ""
        
        random_meal = random.choice(list(set(meal_list)))
        self.removeMealFromList(meal_type, random_meal)
        return random_meal

    def removeMealFromList(self, meal_type, meal):
        """Removes meal from meals list

        Args:
            meal_type (str): meal type/meal time
            meal (str): meal name
        """        
        if meal in self.breakfast_list:
            self.breakfast_list.remove(meal)
        if meal in self.dinner_list:
            self.dinner_list.remove(meal)
        if meal in self.supper_list:
            self.supper_list.remove(meal)
    
    def getMealList(self, meal_type):
        """Get meal list

        Args:
            meal_type (str): meal type/meal time

        Returns:
            list: meal list
        """        
        if "desayuno" == meal_type:
            return self.breakfast_list
        elif "comida" == meal_type:
            return self.dinner_list
        elif "cena" == meal_type:
            return self.supper_list
        else:
            return list()

    def setMealList(self, meal_type):
        """Set meal list

        Args:
            meal_type (str): meal type/meal time

        Returns:
            list: meal list
        """        
        meal_list = list()
        
        for meal, data in self.meals_dict.items():
            if meal_type in data:
                meal_list.append(meal)
        return meal_list
    
class RandomMealCalendar():
    """Generates a calendar with three random meals per day.
    """    
    def __init__(self):
        """Initializes class.
        """        
        self.language = "esp"
        self.random_meal_instance = None
        self.meal_calendar_dict = dict()

        self.setConfigVariables()

    def setConfigVariables(self):
        """Reads config files and initializes variables
        """        
        configs_path = os.path.join(dirname, "configs/{}.json".format(self.language))
        self.configs_dict = utils.readJSON(configs_path)
        self.week_days_list = self.configs_dict.get("week_days", "")
        self.meal_type = self.configs_dict.get("meal_type", "")
        
    def execute(self):
        """Generates random meals for all days of the week.
        """    
        if not self.meal_path:
            return    
        self.random_meal_instance = RandomMeal(self.meal_path)
        
        for day in self.week_days_list:
            self.meal_calendar_dict[day] = dict()
            
            for meal_typ in self.meal_type:
                meal = self.random_meal_instance.getMeal(meal_typ)
                
                self.meal_calendar_dict[day][meal_typ] = dict()
                self.meal_calendar_dict[day][meal_typ] = meal
        
        self.writeCurrentWeekMeal(self.meal_calendar_dict)
    
    def setMealPath(self, path):
        """Sets meal path

        Args:
            path (str): meal json path
        """        
        self.meal_path = path

    def writeCurrentWeekMeal(self, calendar_dict):
        """Writes computed calendar dict into a weekly file

        Args:
            calendar_dict (dict): dict that is going to be dumped into the yaml file
        """        
        today = date.today()
        start = today - timedelta(days=today.weekday())
        end = start + timedelta(days=6)

        week_start = ("").join(str(start).split("-"))
        week_end = ("").join(str(end).split("-"))
        
        week_path = os.path.join(dirname, "weeks/{start}_{end}.yml".format(start=week_start, end=week_end))
        utils.writeYAML(week_path, calendar_dict)
            
    
if __name__ == '__main__':
   random_meal_calendar = RandomMealCalendar()
   meal_path = os.path.join(dirname, 'meals/meals_test.yml')
   random_meal_calendar.setMealPath(meal_path)
   random_meal_calendar.execute()